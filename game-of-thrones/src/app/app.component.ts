import { Component, ViewChild } from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexFill,
  ApexStroke,
  ApexPlotOptions,
  ApexYAxis,
  ApexMarkers,
  ApexLegend

} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
  dataLabels: ApexDataLabels;
};

export type ChartOptions_2 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  title: ApexTitleSubtitle;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  tooltip: any;
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  colors: string[];
  yaxis: ApexYAxis;
  markers: ApexMarkers;
  xaxis: ApexXAxis;
};

export type ChartOptions_3 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  title: ApexTitleSubtitle;
  plotOptions: ApexPlotOptions;
  legend: ApexLegend;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'game-of-thrones';

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  @ViewChild("chart") chart_2: ChartComponent;
  public chartOptions_2: Partial<ChartOptions_2>;


  @ViewChild("chart") chart_3: ChartComponent;
  public chartOptions_3: Partial<ChartOptions_3>;

  constructor() {
    this.chartOptions = {
      series: [
        {
          name: "Kills",
          data: [1008, 199, 68, 51, 34, 31, 27, 22, 20]
        }
      ],
      chart: {
        height: 500,
        type: "bar",
      },
      title: {
        text: "Top Killers",
        style: {
          fontFamily: 'arial',
          fontWeight: 'bold',
          fontSize: '20px'
        }
      },
      xaxis: {
        categories: ["Daenerys Targaaryen", "Cersei Lannister", "Arya Stark", "Jon Snow", "Grey Worm", "Sandor Cleagane", "Bronn", "Daario Naharis", "Tormund"],
        labels:
          {
            trim: true
          }
      },
      dataLabels: {
        enabled: false
      }

    };


    this.chartOptions_2 = {
      series: [
        {
          name: "Anzahl Tode",
          data: [986, 322, 210, 119, 95, 81, 63]
        }
      ],
      chart: {
        height: 400,
        type: "radar"
      },
      dataLabels: {
        enabled: false
      },
      plotOptions: {
        radar: {
          size: 140,
          polygons: {
            fill: {
              colors: ["#f8f8f8", "#fff"]
            }
          }
        }
      },
      title: {
        text: "Top Methods",
        style: {
          fontFamily: 'arial',
          fontWeight: 'bold',
          fontSize: '20px'
        }
      },
      colors: ["#FF4560"],
      markers: {
        size: 4,
        colors: ["#fff"],
        strokeColors: ["#FF4560"],
        strokeWidth: 2
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return val;
          }
        }
      },
      xaxis: {
        categories: [
          "Dragonfire (Dragon)",
          "Sword",
          "Wildfire",
          "Knife",
          "Arrow",
          "Spear",
          "Poison"

        ]
      },
    }

    this.chartOptions_3 = {
      series: [
        {
          name: "Season 8",
          data: this.generateData(10, {
              e1:6,
              e2:1,
              e3:35,
              e4:5,
              e5:844,
              e6:1,
              e7:0,
              e8:0,
              e9:0,
              e10:0,
            }
          )
        },
        {
          name: "Season 7",
          data: this.generateData(10, {
            e1:54,
            e2:29,
            e3:23,
            e4:205,
            e5:4,
            e6:9,
            e7:10,
            e8:0,
            e9:0,
            e10:0,
          })
        },
        {
          name: "Season 6",
          data: this.generateData(10, {
            e1:8,
            e2:7,
            e3:10,
            e4:18,
            e5:8,
            e6:0,
            e7:1,
            e8:8,
            e9:133,
            e10:203,
          })
        },
        {
          name: "Seasion 5",
          data: this.generateData(10, {
            e1:2,
            e2:8,
            e3:5,
            e4:48,
            e5:2,
            e6:1,
            e7:2,
            e8:23,
            e9:58,
            e10:9,
          })
        },
        {
          name: "Season 4",
          data: this.generateData(10, {
            e1:5,
            e2:6,
            e3:11,
            e4:1,
            e5:16,
            e6:7,
            e7:7,
            e8:12,
            e9:86,
            e10:19,
          })
        },
        {
          name: "Season 3",
          data: this.generateData(10, {
            e1:1,
            e2:1,
            e3:4,
            e4:11,
            e5:4,
            e6:7,
            e7:0,
            e8:6,
            e9:45,
            e10:7,
          })
        },
        {
          name: "Season 2",
          data: this.generateData(10, {
            e1:7,
            e2:1,
            e3:8,
            e4:3,
            e5:4,
            e6:12,
            e7:15,
            e8:0,
            e9:72,
            e10:8,
          })
        },
        {
          name: "Season 1",
          data: this.generateData(10, {
            e1:7,
            e2:3,
            e3:0,
            e4:1,
            e5:17,
            e6:5,
            e7:5,
            e8:11,
            e9:6,
            e10:3,
          })
        }
      ],
      chart: {
        height: 500,
        type: "heatmap"
      },
      plotOptions: {
        heatmap: {
          shadeIntensity: 0.5,
          colorScale: {
            ranges: [
              {
                from: 0,
                to: 10,
                name: "low",
                color: "#00A100"
              },
              {
                from: 10,
                to: 50,
                name: "medium",
                color: "#128FD9"
              },
              {
                from: 50,
                to: 100,
                name: "high",
                color: "#FFB200"
              },
              {
                from: 100,
                to: 500,
                name: "extreme",
                color: "#FF0000"
              }
            ]
          }
        }
      },
      dataLabels: {
        enabled: false
      },
      title: {
        text: "Kills by Season and Episode",
        style: {
          fontFamily: 'arial',
          fontWeight: 'bold',
          fontSize: '20px'
        }
      },
      legend:{
        show:true
      }
    };
  }

  public generateData(count, yrange) {
    var i = 0;
    var series = [];
    console.log(Object.values(yrange))
    var episodes = Object.values(yrange);
    for (const episode of episodes) {
      var x = "Episode " + (i + 1).toString();
      var y = episode
      console.log(Object.entries(episode))
      series.push({
        x: x,
        y: y
      });
      i++;
    }


    return series;
  }
}
